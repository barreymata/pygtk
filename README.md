# Learn pygtk
pygtk is a package for develop a GUI apps with GTK by python.
It is a repository where I will add my tests for learning develop pygtk. It will based from examples from http://www.pygtk.org/pygtk2tutorial-es/

## Requisites in Ubuntu.
Python but it's installed by default.

And the pygtk module, for install it: 
```bash
 sudo apt-get install python-gtk2*
```
